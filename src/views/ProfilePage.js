import React from 'react';

class ProfilePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {
                name: 'Karla'
            }
        }
    }

    render() {
        return (
            <section>
              <h1>Hello Okta - My Profile</h1>
              <div>
                <label>Name:</label>
                <span>{this.state.user.name}</span>
              </div>
            </section>
        );
      }
}

export default ProfilePage;