import React from 'react';

class HomePage extends React.Component {
  render() {
    return <h1>Hello Okta - Home Page</h1>;
  }
}

export default HomePage;