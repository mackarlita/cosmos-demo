import UnoButton from ".";

export default [
    {
        name: 'Enabled',
        component: UnoButton,
        props: {
            label: 'Active',
            status: "enabled"
        }
    },
    {
        name: 'Disabled',
        component: UnoButton,
        props: {
            label: 'Disabled',
            status: "disabled"
        }
    },
    {
        name: 'On hover',
        component: UnoButton,
        props: {
            label: 'On Hover',
            status: "hovered"
        }
    },
    {
        name: 'Pressed',
        component: UnoButton,
        props: {
            label: 'Pressed',
            status: "pressed"
        }
    }
]