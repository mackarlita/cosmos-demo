import React from "react";
import axios from "axios";

/** @jsx jsx */
import { jsx, css } from '@emotion/core'

const errorContainer = css`
  color: red;
`;

class Users extends React.Component {

  state = {
    isLoading: true,
    error: null,
    users: null
  };

  componentDidMount() {
    axios('/users')
      .then(({ data: users }) => {
        console.log(users)
        this.setState({ isLoading: false, users });
      })
      .catch(err => {
        console.log(err)
        this.setState({
          isLoading: false,
          error: err.message
        });
      });
  }

  render() {
    const { isLoading, error, users } = this.state;

    if (isLoading) {
      return <div>Loading...</div>;
    }

    if (error) {
      return <div css={errorContainer}>{error}</div>;
    }

    return (
      <ul>
        {users.map(user => (
          <li key={user.id}>{user.name}</li>
        ))}
      </ul>
    );
  }
}

export default Users