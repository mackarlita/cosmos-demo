import Card from ".";
import UnoButton from "../UnoButton"

/** @jsx jsx */
import { jsx, css } from '@emotion/core'

const textElement = (
    <p>
        Minions ipsum bappleees butt tatata bala tu jiji para tú. Chasy daa me want bananaaa!
        Butt belloo! La bodaaa wiiiii poopayee chasy tulaliloo aaaaaah. Jeje gelatooo ti aamoo!
        Potatoooo daa la bodaaa aaaaaah belloo! Aaaaaah. Hahaha gelatooo daa pepete para tú poopayee jeje la bodaaa.
        Bananaaaa tatata bala tu underweaaar daa potatoooo hana dul sae poopayee. Baboiii daa aaaaaah aaaaaah
        jeje tank yuuu! Poulet tikka masala bappleees bappleees ti aamoo! Gelatooo para tú bappleees chasy.
    </p>
);

const containsDoubleColumn = css`
    margin: 0;
    div {
        display: inline-block;
        width: 50%;
    }
`;

export default [
    {
        name: 'Card with Text',
        component: Card,
        props: {
            title: 'Card with Text Only',
            children: (
                textElement
            )
        }
    },
    {
        name: 'Card with Columns',
        component: Card,
        props: {
            title: 'Card with double column',
            children: (
                <div css={containsDoubleColumn}>
                    <div>
                        <p>Fixture ain't afraid of JSX</p>
                        <p>Fixture ain't afraid of nothing!</p>
                    </div>
                    <div>
                        <ul>
                            <li>Item 1</li>
                            <li>Item 2</li>
                            <li>Item 3</li>
                        </ul>
                    </div>
                </div>
            )
        }
    },
    {
        name: 'Card with UnoButtons',
        component: Card,
        props: {
            title: 'Composition Demo',
            children: (
                <div>
                    <div css={css`display:inline-block; padding: 0.5em;`}>
                        <UnoButton label="Click me!" status="enabled"/>
                    </div>
                    <div css={css`display:inline-block; padding: 0.5em;`}>
                        <UnoButton label="Mouse is here!" status="hovered"/>
                    </div>
                    <div css={css`display:inline-block; padding: 0.5em;`}>
                        <UnoButton label="I'm pressed" status="pressed"/>
                    </div>
                    <div css={css`display:inline-block; padding: 0.5em;`}>
                        <UnoButton label="I'm disabled =(" status="disabled"/>
                    </div>
                </div>
            )
        }
    }
]