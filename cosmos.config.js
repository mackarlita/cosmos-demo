module.exports = {
  hostname: "localhost",
  port: 8990,
  rootPath: "src",
  publicPath: "public",
  proxiesPath: "cosmos.proxies.js"
};
